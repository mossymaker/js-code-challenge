class View {
  /**
   * Configure a View instance
   * 
   * @param {Controller} controller
   * @param {Node} target Target node to append to
   * @param {string} templateName Filename of the template to render
   * @param {object} params Hash of parameters
   */
  constructor(controller, target, templateName, params) {
    this.controller = controller;
    this.target = target;
    this.params = params || [];
    this.rootElement = null;
    
    this.controller.onModelUpdate = () => {
      if (this.template) {
        this.render();
      }
    }
    this.loadTemplate(templateName);
  }
  /**
   * Work around webkit's shoddy XSLT implementation by manually loading
   * imported stylesheets and appending them to the given document
   * 
   * @param {Document} document
   */
  static async importChildTemplates(document) {
    const imports = Array.from(document.querySelectorAll('import'));
    const domParser = new DOMParser();
    // Load child templates in parallel
    const childTemplates = await Promise.all(imports.map(async (importElement) => {
      const href = importElement.getAttribute('href');
      let template, templateText;
      // Import is no good in webkit; toss it
      importElement.remove();

      try {
        templateText = await (await fetch(href)).text();
        return domParser.parseFromString(templateText, 'text/xml');
      } catch(ex) {
        console.error(`Couldn't load child template:`, importElement.href);
      }
    }));

    childTemplates.forEach(templateDocument => {
      const template = templateDocument.querySelector('template');
      document.firstChild.appendChild(template);
    });
  }
  /**
   * Load a template with the given name
   * @param {string} name
   */
  async loadTemplate(name) {
    const domParser = new DOMParser();
    let template, templateText;

    try {
      templateText = await (await fetch(`src/templates/${name}`)).text();
      template = domParser.parseFromString(templateText, 'text/xml');
      await View.importChildTemplates(template);

      this.template = template;
      this.render();
    } catch (ex) {
      console.error(ex);
    }
  }
  /**
   * Render the view's template
   */
  render() {
    const xsltProcessor = new XSLTProcessor();
    let xhtml;

    Object.entries(this.params)
      .forEach(([name, value]) => xsltProcessor.setParameter(null, name, value));
    xsltProcessor.importStylesheet(this.template);
    xhtml = xsltProcessor.transformToFragment(this.controller.model, document);

    if (xhtml && xhtml.children && xhtml.children.length > 1) {
      throw Error('View must have a root element');
    }

    if (this.rootElement) {
      this.rootElement.remove();
    }

    if (xhtml) {
      // Calling `appendChild` on a document fragment yields an empty document
      // fragment; append its first child instead so a reference to the node
      // can be kept for removal
      this.rootElement = this.target.appendChild(xhtml.firstChild);
    }
  }
}

export default View;