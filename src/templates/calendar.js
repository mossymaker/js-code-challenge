import View from './base.js';

class CalendarView extends View {
  /**
   * Configure a CalendarView instance
   * 
   * @param {Controller} controller
   * @param {Node} target Target node to append to
   * @param {object} params Hash of parameters
   */
  constructor(controller, target, params) {
    super(controller, target, 'calendar.xsl', params);
  }
}

export default CalendarView;