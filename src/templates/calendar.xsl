<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="src/templates/master.xsl"/>
<xsl:output method="html" encoding="UTF-8"/>
<xsl:param name="day"/>

<xsl:template match="events">
  <h1><xsl:value-of select="$day"></xsl:value-of></h1>
  <div class="calendar">
    <div class="calendar__hours">
      <b>9 am</b><b>10 am</b><b>11 am</b><b>12 pm</b><b>1 pm</b><b>2 pm</b>
      <b>3 pm</b><b>4 pm</b><b>5 pm</b><b>6 pm</b><b>7 pm</b><b>8 pm</b><b>9 pm</b>
    </div>
    <ol class="calendar__day">
      <xsl:for-each select="event">
        <li
          title="{ @startTime } - { @endTime }"
          style="--event-span: { @duration }; --event-start: { @starts_at }; --event-width: { @w }; --event-position: { @position };"
        >
          <xsl:attribute name="class">
            <xsl:text>event </xsl:text>
            <xsl:if test="number(@duration) &lt; 60">
              <xsl:text>event--compact</xsl:text>
            </xsl:if>
          </xsl:attribute>
          <xsl:if test="@title">
            <span class="event__title">
              <xsl:value-of select="@title"/>
            </span>
          </xsl:if>
          <xsl:if test="@location">
            <span class="event__location">
              <xsl:value-of select="@location"/>
            </span>
          </xsl:if>
          <span class="event__time">
            <xsl:value-of select="@startTime"/> - <xsl:value-of select="@endTime"/>
          </span>
        </li>  
      </xsl:for-each>
    </ol>
  </div>
</xsl:template>
</xsl:stylesheet>