class Controller {
  constructor(model) {
    this.model = model;
  }

  get model() {
    return this._model;
  }
  set model(model) {
    this._model = model;
    this.onModelUpdate();
  }

  onModelUpdate() {}
}

export default Controller;