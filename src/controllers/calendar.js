import Controller from './base.js';
import eventsTransform from '../transforms/event.js';

const MAX_START_TIME = 720;

class CalendarController extends Controller {
  get model() {
    return super.model;
  }
  set model(events) {
    // TODO: consider filtering at the "API layer"
    const filteredEvents = events.filter(e => e.starts_at <= MAX_START_TIME);
    super.model = eventsTransform(filteredEvents);
  }
}

export default CalendarController;