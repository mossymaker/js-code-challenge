const express = require('express');
const app = express();

const PORT = 3000;

app.use('/src', express.static('src'));
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.listen(PORT);
console.log(`Listening on http://localhost:${PORT}`);
